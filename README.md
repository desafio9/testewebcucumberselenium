**Desafio Web Selenium**

Testes automatizados dos serviços disponibilizados pela url: http://sampleapp.tricentis.com/101/app.php<br>

**Objetivo:**

Incluir uma cotação de seguro no sistema.<br>

**Recursos utilizados:**

• Selenium-java; <br>
• JUnit; <br>
• Maven; <br>
• Cucumber; <br>
• Java 8. <br>

**Como configurar o ambiente:**

• Faça clone do projeto: https://gitlab.com/desafio9/testewebselenium.git; <br>
• Importe o projeto para sua IDE de preferência; <br>
• Caso não utilize o Chrome versão 89, alterar o arquivo chromedriver na pasta “src/Driver” para a versão do Chrome que for utilizar; <br>
• Necessário ter instalado o JDK8. <br>

**Como executar a aplicação:**

• Executar o cenário na pasta src/test/resources/feature <br>

**Descrição do Projeto:**

• Pasta src/test/java/given: Contém a execução do teste " Given que fiz acesso para contratacao"; <br>
• Pasta src/test/java/tests: Contém a execução dos testes "When inserir dados aba Enter Vehicle Data, And inserir dados aba Enter Insurant Data, <br>
And inserir dados aba Enter Product Data, And inserir dados aba Select Price Option, And inserir dados aba Send Quote"; <br>
Then valido a mensagem de confirmacao e finalizo o envio.<br>
                                                                
• Pasta src/Driver: Contém o chromedriver versão 89. <br>

