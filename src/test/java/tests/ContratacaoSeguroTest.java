package tests;

import given.ContratacaoSeguro;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.assertEquals;
import static org.openqa.selenium.By.cssSelector;

public class ContratacaoSeguroTest {

    ContratacaoSeguro contratacaoSeguro = new ContratacaoSeguro();
    public static WebDriver navegador;

    @When("inserir dados aba Enter Vehicle Data")
    public void inserir_dados_Vehicle() throws Throwable {

        WebDriverWait wait = new WebDriverWait(contratacaoSeguro.navegador, 5);
        WebElement marca = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("make")));
        marca.sendKeys("Honda");

        contratacaoSeguro.navegador.findElement(By.id("model")).sendKeys("Motorcycle");
        contratacaoSeguro.navegador.findElement(By.id("cylindercapacity")).sendKeys("500");
        contratacaoSeguro.navegador.findElement(By.id("engineperformance")).sendKeys("500");
        contratacaoSeguro.navegador.findElement(By.id("dateofmanufacture")).sendKeys("01/30/2021");
        contratacaoSeguro.navegador.findElement(By.id("numberofseats")).sendKeys("2");
        contratacaoSeguro.navegador.findElement(cssSelector("#insurance-form > div > section:nth-child(1) > div:nth-child(7) > p > label:nth-child(1) > span")).click();
        contratacaoSeguro.navegador.findElement(By.id("numberofseatsmotorcycle")).sendKeys("2");
        contratacaoSeguro.navegador.findElement(By.id("fuel")).sendKeys("Other");
        contratacaoSeguro.navegador.findElement(By.id("payload")).sendKeys("20");
        contratacaoSeguro.navegador.findElement(By.id("totalweight")).sendKeys("200");
        contratacaoSeguro.navegador.findElement(By.id("listprice")).sendKeys("9000");
        contratacaoSeguro.navegador.findElement(By.id("licenseplatenumber")).sendKeys("AAA0000");
        contratacaoSeguro.navegador.findElement(By.id("annualmileage")).sendKeys("100000");
        contratacaoSeguro.navegador.findElement(By.id("nextenterinsurantdata")).click();
    }
    @And("inserir dados aba Enter Insurant Data")
    public void inserir_dados_Insurante() throws Throwable {

        WebDriverWait wait = new WebDriverWait(contratacaoSeguro.navegador, 5);
        WebElement marca = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("firstname")));
        marca.sendKeys("Janaina");

        contratacaoSeguro.navegador.findElement(By.id("lastname")).sendKeys("Lima");
        contratacaoSeguro.navegador.findElement(By.id("birthdate")).sendKeys("07/27/1983");
        contratacaoSeguro.navegador.findElement(cssSelector("#insurance-form > div > section:nth-child(2) > div:nth-child(4) > p > label:nth-child(2) > span")).click();
        contratacaoSeguro.navegador.findElement(By.id("country")).sendKeys("Brazil");
        contratacaoSeguro.navegador.findElement(By.id("zipcode")).sendKeys("91110000");
        contratacaoSeguro.navegador.findElement(By.id("occupation")).sendKeys("Employee");
        contratacaoSeguro.navegador.findElement(cssSelector("#insurance-form > div > section:nth-child(2) > div.field.idealforms-field.idealforms-field-checkbox > p > label:nth-child(1) > span")).click();
        contratacaoSeguro.navegador.findElement(By.id("nextenterproductdata")).click();
    }
    @And("inserir dados aba Enter Product Data")
    public void inserir_dados_Product() throws Throwable {

        WebDriverWait wait = new WebDriverWait(contratacaoSeguro.navegador, 5);
        WebElement marca = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("startdate")));
        marca.sendKeys("06/30/2021");

        contratacaoSeguro.navegador.findElement(By.id("insurancesum")).sendKeys("3.000.000,00");
        contratacaoSeguro.navegador.findElement(By.id("meritrating")).sendKeys("Bonus 1");
        contratacaoSeguro.navegador.findElement(By.id("damageinsurance")).sendKeys("Full Coverage");
        contratacaoSeguro.navegador.findElement(cssSelector("#insurance-form > div > section:nth-child(3) > div.field.idealforms-field.idealforms-field-checkbox > p > label:nth-child(1)")).click();
        contratacaoSeguro.navegador.findElement(By.id("courtesycar")).sendKeys("Yes");
        contratacaoSeguro.navegador.findElement(By.id("nextselectpriceoption")).click();
    }
    @And("inserir dados aba Select Price Option")
    public void inserir_dados_Price() throws Throwable {

        WebDriverWait wait = new WebDriverWait(contratacaoSeguro.navegador, 5);
        WebElement marca = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#priceTable > tfoot > tr > th.group > label:nth-child(4) > span")));
        marca.click();

        contratacaoSeguro.navegador.findElement(By.id("nextsendquote")).click();
    }
    @And("inserir dados aba Send Quote")
    public void inserir_dados_Send_Quote() throws Throwable {
        WebDriverWait wait = new WebDriverWait(contratacaoSeguro.navegador, 5);
        WebElement marca = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("email")));
        marca.sendKeys("janaina@gmail.com");

        contratacaoSeguro.navegador.findElement(By.id("username")).sendKeys("janaina");
        contratacaoSeguro.navegador.findElement(By.id("password")).sendKeys("Teste123");
        contratacaoSeguro.navegador.findElement(By.id("confirmpassword")).sendKeys("Teste123");
        contratacaoSeguro.navegador.findElement(By.id("sendemail")).click();
    }

    @Then("valido a mensagem de confirmacao e finalizo o envio")
    public void valido_mensagem_confirmacao_finalizo_envio() throws Throwable {
        WebDriverWait wait = new WebDriverWait(contratacaoSeguro.navegador, 5);
        WebElement msgsucessconfirm = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("body > div.sweet-alert.showSweetAlert.visible > h2")));
        String msgconfirm = msgsucessconfirm.getText();
        assertEquals("Sending e-mail success!", msgconfirm);
        contratacaoSeguro.navegador.findElement(By.className("confirm")).click();
        contratacaoSeguro.navegador.quit();}
}
