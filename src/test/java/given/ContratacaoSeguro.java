package given;

import io.cucumber.java.en.Given;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class ContratacaoSeguro {
    public static WebDriver navegador;

    @Given("que fiz acesso para contratacao")
    public void que_fiz_acesso_para_contratacao() throws Throwable {
        System.setProperty("webdriver.chrome.driver", "src/Driver/chromedriver.exe");
        navegador = new ChromeDriver();
        navegador.manage().window().maximize();
        navegador.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        navegador.get("http://sampleapp.tricentis.com/101/app.php");
    }

}
